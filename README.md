# gridval

Ce mini projet de tests permet de valider la capacité du grid Selenium de la PIC à initialiser :
* un test Selenium avec le navigateur Firefox
* un test Selenium avec le navigateur Chromium
* une exécution de ZAP

Ces tests se contentent d'accéder à la console du grid selenium lui même.

## Paramétrage du projet
Le fichier src/test/resources/<nom_du_fichier>.properties permet de paramétrer les adresses et ports à utiliser pour accéder aux services Selenium et ZAP.


Exemple de fichier pic.nubo.selenium.properties :
grid.host=selenium.pic.soda.dgfip  
grid.port=4444  
zap.adresse=zap.pic.soda.dgfip  
zap.port=7777  
zap.apikey=desactivate  
application.baseUrl=selenium.pic.soda.dgfip  
application.portUrl=4444  

pic.nubo.selenium.properties est le fichier de paramétrage par défaut du projet de test.
Il est possible d'utiliser un fichier de paramétrage alternatif en fournissant en paramètre d'exécution de la VM le paramètre "selenium.properties.filename"

Par exemple, pour utiliser le fichier de paramètre local.selenium.properties, il faut ajouter dans les paramètres d'exécution de la VM  :
-Dselenium.properties.filename=pic.nubo.selenium.properties

## Exécution dans Jenkins
### Utilisation d'un Job de type Maven
* Configuration du Build  
** Version de Maven 3.5+  
** POM Racine : pom.xml  
** Goals et options : install  
** MAVEN_OPTS : -Dselenium.properties.filename=<nom du fichier de paramétrage à utiliser>  

### Utilisation d'un pipeline

