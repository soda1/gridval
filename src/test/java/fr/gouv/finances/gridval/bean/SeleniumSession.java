package fr.gouv.finances.gridval.bean;

import java.net.URL;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.google.common.flogger.FluentLogger;

import fr.gouv.finances.gridval.utils.UrlContext;

public class SeleniumSession extends DataContext
{
	private static final FluentLogger LOGGER = FluentLogger.forEnclosingClass();
	
    protected static boolean activateZap;

    protected final static String FIREFOX = "firefox";
    
    protected final static String CHROME = "chrome";

    public static boolean IS_HEADLESS_DEFAULT = false;

    protected String browser;

    protected boolean isFirefox;

    protected MutableCapabilities options;

    protected WebDriver webDriver;

    protected EventFiringWebDriver driver;

    public SeleniumSession(String browser)
    {
    	LOGGER.atInfo().log("Début initialisation d'une session Selenium ");
    	
        isFirefox = FIREFOX.equalsIgnoreCase(browser);
        
        
        LOGGER.atInfo().log("Navigateur à utiliser : %s ", browser);
        
        if (isFirefox)
        {
            IS_HEADLESS_DEFAULT = true;
                     
    		FirefoxOptions firefoxOption = new FirefoxOptions();
    		
    		firefoxOption.setCapability(CapabilityType.BROWSER_NAME, browser);
    	    		
    		LoggingPreferences  logPrefs = new LoggingPreferences();
    		logPrefs.enable(LogType.BROWSER, Level.ALL);
    		logPrefs.enable(LogType.DRIVER, Level.ALL);
    		logPrefs.enable(LogType.SERVER, Level.ALL);
    		firefoxOption.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
    		firefoxOption.setHeadless(IS_HEADLESS_DEFAULT);

            if (activateZap)
            {
                ((FirefoxOptions) options).setProxy(new Proxy().setHttpProxy(adressZap + ":" + portZap));
            }
            
            options = firefoxOption;
        }
        else
        {
    		ChromeOptions chromeOptions = new ChromeOptions();
    		
    		
    		chromeOptions.setCapability(CapabilityType.BROWSER_NAME, browser);

    		LoggingPreferences logPrefs = new LoggingPreferences();
    		logPrefs.enable(LogType.BROWSER, Level.ALL);
    		logPrefs.enable(LogType.DRIVER, Level.ALL);
    		logPrefs.enable(LogType.SERVER, Level.ALL);
    		chromeOptions.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
    	//	chromeOptions.setCapability(CapabilityType.ENABLE_PROFILING_CAPABILITY, true);
    		chromeOptions.setHeadless(true);
        	
        //	chOptions.addArguments("user-data-dir=/tmp");
        	        	
        	options = chromeOptions;
        }
        
        
        URL url = UrlContext.getUrlGrid(hostGrid, portGrid, "/wd/hub");
        
        LOGGER.atInfo().log("Url d'initialisation du remote webdriver %s", url.toString());
        
        webDriver = new RemoteWebDriver(url, options);
        
        
        ListenerSelenium listener = new ListenerSelenium();
        driver = new EventFiringWebDriver(webDriver);
        driver.register(listener);       
        
        this.traceCapabilities();
    }

	private void traceCapabilities() {
		Capabilities caps = driver.getCapabilities();
  
		
        StringBuilder infoDriver = new StringBuilder();
        
        Map<String,Object>  capaMap = caps.asMap();
        Set<String> cles = capaMap.keySet();
        infoDriver.append("\n");
        for (String cle : cles) {
        	infoDriver.append(cle);
        	infoDriver.append(" = ");
        	infoDriver.append(capaMap.get(cle));
        	infoDriver.append("\n");
		}
        
        LOGGER.atInfo().log(infoDriver.toString());
	}

    public SeleniumSession()
    {
    	LOGGER.atInfo().log("Début initialisation d'une session Selenium - firefox");
    	
        options = new FirefoxOptions().setHeadless(IS_HEADLESS_DEFAULT);
        
        if (activateZap)
        {
            ((FirefoxOptions) options).setProxy(new Proxy().setHttpProxy(adressZap + ":" + portZap));
        }
        URL url = UrlContext.getUrlGrid(hostGrid, portGrid, "/wd/hub");
        
        LOGGER.atInfo().log("url du remotewebdriver = %s", url.toString());
        webDriver = new RemoteWebDriver(url, options);
        
        
        ListenerSelenium listener = new ListenerSelenium();
        LOGGER.atInfo().log("Instanciation du driver");
        driver = new EventFiringWebDriver(webDriver);
        
        LOGGER.atInfo().log("Début d'enregistrement du web driver auprès du listener");
        driver.register(listener);
        
        this.traceCapabilities();
    	
    }

}
