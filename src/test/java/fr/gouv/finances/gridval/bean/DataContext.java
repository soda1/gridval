package fr.gouv.finances.gridval.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.Platform;

import com.google.common.flogger.FluentLogger;
import com.google.common.flogger.StackSize;

public class DataContext extends KeysProperties
{

    private static final FluentLogger LOGGER = FluentLogger.forEnclosingClass();

    public static boolean IS_NUBO = false;

    public static String baseUrl;

    public static String portUrl;
    
    public static String hostGrid;

    public static String portGrid;

    public static Platform platformCible;

    public static String adressZap;

    public static int portZap;

    public static String apiKeyZap;

    public static String versionFirefox;

    public static String versionChrome;
    
    public static String chromeName = "chrome";



	static
    {
        try
        {
            loadProperties();
            LogProperties();
        }
        catch (IOException e)
        {
            LOGGER.atWarning().withStackTrace(StackSize.FULL).log(
                "Step valorisation des données du fichier properties %s",
                System.getProperty("selenium.properties.filename"));
        }
    }

    
    public static void LogProperties()
    {
    	StringBuilder infoProperties = new StringBuilder();
    	
    	
    	infoProperties.append("IS_NUBO = ");
    	infoProperties.append(IS_NUBO);
    	infoProperties.append("\n");
    	infoProperties.append("hostGrid = ");
    	infoProperties.append(hostGrid);
    	infoProperties.append("\n");
    	infoProperties.append("portGrid = ");
    	infoProperties.append(portGrid);
    	infoProperties.append("\n");
    	infoProperties.append("platformCible = ");
    	infoProperties.append(platformCible);
    	infoProperties.append("\n");
    	infoProperties.append("adressZap = ");
    	infoProperties.append(adressZap);
    	infoProperties.append("\n");
    	infoProperties.append("portZap = ");
    	infoProperties.append(portZap);
    	infoProperties.append("\n");
    	infoProperties.append("apiKeyZap = ");
    	infoProperties.append(apiKeyZap);
    	infoProperties.append("\n");
    	infoProperties.append("versionFirefox = ");
    	infoProperties.append(versionFirefox);
    	infoProperties.append("\n");
    	infoProperties.append("versionChrome = ");
    	infoProperties.append(versionChrome);
    	infoProperties.append("\n");
    	infoProperties.append("baseUrl = ");
    	infoProperties.append(baseUrl);
    	infoProperties.append("\n");
    	infoProperties.append("portUrl = ");
    	infoProperties.append(portUrl);
    	infoProperties.append("\n");
    	LOGGER.atInfo().log(infoProperties.toString());
    }
    
    /**
     * @param filename
     * @throws IOException
     */
    public static void loadProperties() throws IOException
    {
        String filename = System.getProperty(keyProperties, "pic.nubo.selenium.properties");
        Properties prop = new Properties();
        InputStream initialStreamGrid;
        final String resourceTests = "/" + filename;
        LOGGER.atInfo().log("Fichier de configuration injecte: %s", filename);
        File fileProperties = new File(filename);
        if (fileProperties.exists())
        {
            initialStreamGrid = new FileInputStream(fileProperties);
        }
        else
        {
            initialStreamGrid = DataContext.class.getResourceAsStream(resourceTests);
        }
        if (initialStreamGrid != null)
        {
            if (filename.contains("nubo"))
            {
                IS_NUBO = true;
                chromeName = "chromium";
            }
            prop.load(initialStreamGrid);
            hostGrid = prop.getProperty(keyHostGridSelenium, "localhost");
            portGrid = prop.getProperty(keyPortGridSelenium, "4444");
            
            adressZap = prop.getProperty(keyHostZap, "localhost");
            portZap = Integer.parseInt(prop.getProperty(keyPortZap, "6666"));
         
            baseUrl = prop.getProperty(keyBaseUrl, "selenium.pic.soda.dgfip");
            portUrl = prop.getProperty(keyPortUrl, "0");
         
            
            
            // on valorise par défaut sans Key pour accéder à l'API
            apiKeyZap = prop.getProperty(keyApikeyZap, "");
            if ("desactivate".equals(apiKeyZap))
            {
                apiKeyZap = null;
            }
            versionFirefox = prop.getProperty(keyFirefoxVersion, "");
            versionChrome = prop.getProperty(keyChromeVersion, "");
        }

    }
    

}
