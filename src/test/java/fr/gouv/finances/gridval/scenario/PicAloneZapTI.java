package fr.gouv.finances.gridval.scenario;

import java.net.URL;

import org.junit.Test;

import com.google.common.flogger.FluentLogger;

import fr.gouv.finances.gridval.bean.DataContext;
import fr.gouv.finances.gridval.utils.EncodeDecode;
import fr.gouv.finances.gridval.utils.UrlContext;
import fr.gouv.finances.gridval.zap.ZapApi;
import org.junit.Assert;

public class PicAloneZapTI extends DataContext
{

    private static final FluentLogger LOGGER = FluentLogger.forEnclosingClass();

    @Test
    public void analyseZapAlone()
    {
		String ipOrVirtualHost= "icetea.appli.dgfip";
		
        if ("icetea.appli.dgfip".equals(baseUrl))
        {
            ipOrVirtualHost = new String(EncodeDecode.decode("MTcyLjIyLjI0OC4z"));
        }
        else
        {
            ipOrVirtualHost = new String(baseUrl);
        }
    
        URL urlAccessApplication = UrlContext.getApplicationUrl(ipOrVirtualHost, portUrl, "/grid/console");	
    	
        ZapApi.debug = true;
        LOGGER.atInfo().log("Lancement de ZAP: %s ; %s ; %s ; %s", urlAccessApplication, portZap, adressZap, apiKeyZap);
        ZapApi.runSpider(urlAccessApplication.toString(), portZap, adressZap, apiKeyZap);
        String idScan = ZapApi.runScan(urlAccessApplication.toString(), portZap, adressZap, apiKeyZap);
        
        LOGGER.atInfo().log("Scan numero: %s", idScan);
        Assert.assertTrue(! "-1".equals(idScan));
        
        boolean flagReportWrited = ZapApi.getAndWriteZapReport(urlAccessApplication.toString(), portZap, adressZap, apiKeyZap);
        Assert.assertTrue(flagReportWrited);
    }

}
