package fr.gouv.finances.gridval.scenario;

import java.io.File;
import java.io.IOException;

import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeDriverService.Builder;
import org.openqa.selenium.chrome.ChromeOptions;

import com.google.common.flogger.FluentLogger;

public class TestDirectChromeDriver {

	private static final FluentLogger LOGGER = FluentLogger.forEnclosingClass();
	

	public void runChromeDriver() {
		// WebDriverManager.chromedriver().setup();

		// ChromeDriver.builder().
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		ChromeDriver webDriver = null;//= new ChromeDriver();
		ChromeOptions options = new ChromeOptions();

	
		options.addArguments("--enable-automation"); // https://stackoverflow.com/a/43840128/1689770
		options.addArguments("--headless"); // only if you are ACTUALLY running headless
		options.addArguments("--no-sandbox"); // https://stackoverflow.com/a/50725918/1689770
		options.addArguments("--disable-infobars"); // https://stackoverflow.com/a/43840128/1689770
		options.addArguments("--disable-dev-shm-usage"); // https://stackoverflow.com/a/50725918/1689770
		options.addArguments("--disable-gpu"); // https://stackoverflow.com/questions/51959986/how-to-solve-selenium-chromedriver-timed-out-receiving-message-from-renderer-exc
		options.addArguments("--window-size=1920,1080");
		options.addArguments("--start-maximized");
		options.addArguments("--disable-extensions");
		options.setExperimentalOption("useAutomationExtension", false);
		options.addArguments("--bwsi");
	//	options.addArguments("--user-data-dir=" + System.getProperty("test.web.chrome.data.dir") + "/user-data-dir");
	//	options.addArguments("--disk-cache-dir=" + System.getProperty("test.web.chrome.data.dir") + "/disk-cache-dir");
		try {
			File logFile = new File("/tmp/chromedriver.log");
		//			System.getProperty("test.web.chrome.data.dir").replace("/", "\\") + "\\logs\\chromedriver.log");
			logFile.getParentFile().mkdirs();
			logFile.createNewFile();
			//boolean verbose = System.getProperty("test.web.webdriver.verbose").equals("true");
			ChromeDriverService driverService = new Builder().withVerbose(true).withLogFile(logFile).build();

			webDriver = new ChromeDriver(driverService, options);
		} catch (IOException ioe) {
			LOGGER.atInfo().log("Erreur", ioe);
			webDriver = new ChromeDriver(options);
		}
		finally
		{
			if (webDriver != null)
			{
				webDriver.quit();
			}
		}

	}
}
