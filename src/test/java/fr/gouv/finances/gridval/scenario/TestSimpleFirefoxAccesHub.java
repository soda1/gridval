package fr.gouv.finances.gridval.scenario;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.flogger.FluentLogger;

import fr.gouv.finances.gridval.bean.SeleniumSession;
import fr.gouv.finances.gridval.utils.EncodeDecode;
import fr.gouv.finances.gridval.utils.UrlContext;

public class TestSimpleFirefoxAccesHub  extends SeleniumSession {

    static
    {
        activateZap = false;       
    }

	
	public TestSimpleFirefoxAccesHub() {
		super(FIREFOX);
	}

	private static final FluentLogger LOGGER = FluentLogger.forEnclosingClass();

	//private WebDriver driver;
	private URL urlAccessApplication;

	@Before
	public void setUp() throws MalformedURLException {


		String ipOrVirtualHost= "icetea.appli.dgfip";
		
        if ("icetea.appli.dgfip".equals(baseUrl))
        {
            ipOrVirtualHost = new String(EncodeDecode.decode("MTcyLjIyLjI0OC4z"));
        }
        else
        {
            ipOrVirtualHost = new String(baseUrl);
        }
    
         urlAccessApplication = UrlContext.getApplicationUrl(ipOrVirtualHost, portUrl, "/grid/console");	

	}

	@After
	public void afterTest() {
		if (driver != null) {
			driver.quit();
		}
	}

	@Test
	public void sampleFirefoxTest() {

		LOGGER.atInfo().log("début  du test Firefox : %s", urlAccessApplication.toString());
		driver.get(urlAccessApplication.toString());

		LOGGER.atInfo().log(driver.getPageSource());
		
		if (driver.getPageSource().contains("Grid")) {
			Assert.assertTrue(true);
		} else {
			LOGGER.atInfo().log(driver.getPageSource());
			Assert.assertTrue(false);;
		}

		LOGGER.atInfo().log("fin setup du test firefox");
	}

}
